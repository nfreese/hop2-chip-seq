# HOP2::HA ChIP-Seq project #

### Experimental Design ###

This repository contains project files and scripts used to process and analyze ChIP-Seq data.

### Add QuickLoad to IGB ###

BAM, bedgraph coverage, narrowPeak, and summit files can be accessed in the Integrated Genome Browser (IGB) by adding them as a QuickLoad.

**To add the QuickLoad to IGB:**

1. Download and install IGB by visiting [https://www.bioviz.org/](https://www.bioviz.org/)
2. Launch IGB.
3. In IGB, select File > Preferences...
4. In the Preferences window, select the Data Sources tab.
5. Click the “Add…” button to open the Add Data Source window.
6. Paste the following link in the URL text box: https://bitbucket.org/nfreese/hop2-chip-seq/raw/main/quickload/
7. Click Submit.
8. Open the Arabidopsis thaliana genome. 

The data should appear in the Data Access tab in the Available Data box.

### Contact ###

* Nowlan Freese (nowlan.freese@gmail.com)
* Dan Riggs (dan.riggs@utoronto.ca)